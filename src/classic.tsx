import { StrictMode } from "react";
import { useClassicRunner } from "@hooks/runner";
import { IClassicUIProps } from "@interfaces/props";
import { ClassicRoot } from "@ui/root";
import { Navigation } from "@ui/navigation";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { Buttons } from "@ui/navigation/buttons";
import { Progressbar } from "@ui/navigation/progressbar";
import { Pages } from "@ui/navigation/pages";
import { EvaluatingMessage } from "@ui/messages/evaluating";
import { EmptyMessage } from "@ui/messages/empty";
import { ClosedMessage } from "@ui/messages/closed";
import { PausedMessage } from "@ui/messages/paused";
import { Prologue } from "@ui/messages/prologue";
import { Epilogue } from "@ui/messages/epilogue";
import { Banner } from "@ui/messages/banner";
import { ConnectionError } from "@ui/errors/connection";
import { OutdatedError } from "@ui/errors/outdated";
import { RejectedError } from "@ui/errors/rejected";
import { PauseError } from "@ui/errors/pause";
import { PauseBlock } from "@ui/blocks/pause";
import { PreviousIcon } from "@ui/icons/previous";
import { NextIcon } from "@ui/icons/next";
import { FinishIcon } from "@ui/icons/finish";
import { RetryIcon } from "@ui/icons/retry";
import { PauseIcon } from "@ui/icons/pause";
import "@blocks";

export const ClassicUI = (props: IClassicUIProps) => {
    const {
        status,
        view,
        mode,
        frameRef,
        title,
        l10n,
        styles,
        prologue,
        blocks,
        buttons,
        pausing,
        progressPercentage,
        pages,
        epilogue,
        isEvaluating,
        isPage,
        tabIndex,
        OverlayProvider,
    } = useClassicRunner(props);

    return (
        <StrictMode>
            <ClassicRoot
                frameRef={frameRef}
                view={view}
                isPage={isPage}
                title={title}
                styles={styles}
                className={props.className}
                customStyle={props.customStyle}
                customCSS={props.customCSS}
                onTouch={props.onTouch}
            >
                {(prologue && <Prologue {...prologue} />) ||
                    (blocks && (
                        <>
                            {blocks}
                            {isEvaluating && <EvaluatingMessage l10n={l10n} styles={styles} />}
                            {status === "error" && <ConnectionError l10n={l10n} styles={styles} />}
                            {status === "error-outdated" && (
                                <OutdatedError l10n={l10n} styles={styles} allowReload={props.onReload ? true : false} />
                            )}
                            {status === "error-rejected" && <RejectedError l10n={l10n} styles={styles} />}
                            {status === "error-paused" && <PauseError l10n={l10n} styles={styles} />}
                            {buttons && (
                                <Navigation>
                                    <Buttons>
                                        {mode === "progressive" ? (
                                            <ButtonFabric
                                                styles={styles.finishButton}
                                                tabIndex={tabIndex}
                                                disabled={status === "finishing" || status === "reloading" || !buttons.finishable}
                                                icon={
                                                    status === "error" ||
                                                    (status === "error-outdated" && props.onReload) ||
                                                    status === "reloading"
                                                        ? RetryIcon
                                                        : FinishIcon
                                                }
                                                label={
                                                    status === "finishing"
                                                        ? l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                                                        : (status === "error-outdated" && props.onReload) || status === "reloading"
                                                          ? l10n.pgettext("runner#1|🆗 Buttons", "Reload")
                                                          : status === "error"
                                                            ? l10n.pgettext("runner#1|🆗 Buttons", "Retry")
                                                            : l10n.pgettext("runner#1|🆗 Buttons", "Submit")
                                                }
                                                onClick={buttons.reload || buttons.finish}
                                            />
                                        ) : (
                                            <>
                                                <ButtonFabric
                                                    styles={buttons.finish ? styles.finishButton : styles.buttons}
                                                    tabIndex={tabIndex}
                                                    disabled={
                                                        status === "finishing" ||
                                                        status === "reloading" ||
                                                        (buttons.finish && !buttons.finishable)
                                                    }
                                                    icon={
                                                        status === "error" ||
                                                        (status === "error-outdated" && props.onReload) ||
                                                        status === "reloading"
                                                            ? RetryIcon
                                                            : buttons.finish
                                                              ? FinishIcon
                                                              : NextIcon
                                                    }
                                                    label={
                                                        status === "finishing"
                                                            ? l10n.pgettext("runner#3|🩺 Status information", "Submitting...")
                                                            : buttons.finish
                                                              ? (status === "error-outdated" && props.onReload) || status === "reloading"
                                                                  ? l10n.pgettext("runner#1|🆗 Buttons", "Reload")
                                                                  : status === "error"
                                                                    ? l10n.pgettext("runner#1|🆗 Buttons", "Retry")
                                                                    : l10n.pgettext("runner#1|🆗 Buttons", "Submit")
                                                              : l10n.pgettext("runner#1|🆗 Buttons", "Next")
                                                    }
                                                    onClick={buttons.reload || buttons.next}
                                                />
                                                {buttons.previous && (
                                                    <ButtonFabric
                                                        styles={{ ...styles.buttons, mode: "outline" }}
                                                        tabIndex={tabIndex}
                                                        disabled={status === "finishing"}
                                                        icon={PreviousIcon}
                                                        label={l10n.pgettext("runner#1|🆗 Buttons", "Back")}
                                                        onClick={buttons.previous}
                                                    />
                                                )}
                                            </>
                                        )}
                                        {buttons.pause && (
                                            <ButtonFabric
                                                styles={{ ...styles.buttons, mode: "outline" }}
                                                tabIndex={tabIndex}
                                                disabled={status === "finishing"}
                                                icon={PauseIcon}
                                                onClick={buttons.pause}
                                            />
                                        )}
                                    </Buttons>
                                    {mode === "paginated" && styles.showProgressbar && (
                                        <Progressbar percentage={progressPercentage} styles={styles} />
                                    )}
                                    {mode === "paginated" && styles.showPageIndicators && (
                                        <Pages pages={pages} styles={styles} disabled={status === "finishing"} />
                                    )}
                                </Navigation>
                            )}
                            <PauseBlock controller={pausing} l10n={l10n} styles={styles} view={view} />
                            {(view === "live" || (view === "test" && status !== "empty")) && <Banner l10n={l10n} styles={styles} />}
                        </>
                    )) ||
                    (epilogue && <Epilogue {...epilogue} />) ||
                    (view !== "live" && (status === "empty" || status === "preview") && (
                        <EmptyMessage l10n={l10n} styles={styles} view={view} />
                    )) ||
                    (status === "paused" && <PausedMessage l10n={l10n} styles={styles} view={view} isPage={isPage} />) || (
                        <ClosedMessage l10n={l10n} styles={styles} view={view} isPage={isPage} />
                    )}
                <OverlayProvider />
            </ClassicRoot>
        </StrictMode>
    );
};
