declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Namespace identifier for the runner. */
export const namespace = `${PACKAGE_NAME}@${PACKAGE_VERSION}`;
