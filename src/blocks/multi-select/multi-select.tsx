import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { MultiSelect } from "@tripetto/block-multi-select/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { MultiSelectFabric } from "@tripetto/runner-fabric/components/multi-select";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-multi-select",
})
export class MultiSelectBlock extends MultiSelect implements IClassicRendering {
    render(props: IClassicRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <MultiSelectFabric
                    id={props.id}
                    overlay={props.overlay}
                    styles={props.styles.inputs}
                    options={this.options(props)}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
                {props.ariaDescription}
            </>
        );
    }
}
