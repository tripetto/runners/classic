import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Text } from "@tripetto/block-text/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { TextFabric } from "@tripetto/runner-fabric/components/text";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-text",
})
export class TextBlock extends Text implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <TextFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.textSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    maxLength={this.maxLength}
                    autoComplete={this.autoComplete}
                    suggestions={this.suggestions}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
