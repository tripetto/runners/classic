import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Matrix } from "@tripetto/block-matrix/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { MatrixFabric } from "@tripetto/runner-fabric/components/matrix";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-matrix",
})
export class MatrixBlock extends Matrix implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <MatrixFabric
                    styles={props.styles.matrix}
                    columns={this.columns(props)}
                    rows={this.rows(props)}
                    ariaDescribedBy={props.ariaDescribedBy}
                    allowUnselect={true}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
