import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Number } from "@tripetto/block-number/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { NumberFabric } from "@tripetto/runner-fabric/components/number";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-number",
})
export class NumberBlock extends Number implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <NumberFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    l10n={props.l10n}
                    value={this.numberSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
