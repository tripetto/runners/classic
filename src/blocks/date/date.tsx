import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { DateTime } from "@tripetto/block-date/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { DateTimeFabric } from "@tripetto/runner-fabric/components/datetime";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-date",
})
export class DateTimeBlock extends DateTime implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <DateTimeFabric
                    id={props.id}
                    mode={this.props.time ? "datetime" : "date"}
                    styles={props.styles.inputs}
                    l10n={props.l10n}
                    value={this.dateSlot}
                    required={this.required}
                    error={props.isFailed && (!this.props.range || this.fromError)}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={(!this.props.range && props.onSubmit) || undefined}
                />
                {this.props.range && this.toSlot && (
                    <DateTimeFabric
                        mode={this.props.time ? "datetime" : "date"}
                        styles={props.styles.inputs}
                        l10n={props.l10n}
                        value={this.toSlot}
                        required={this.required}
                        error={props.isFailed && (!this.props.range || this.toError)}
                        tabIndex={props.tabIndex}
                        placeholder={this.toPlaceholder}
                        ariaDescribedBy={props.ariaDescribedBy}
                        onFocus={props.focus}
                        onBlur={props.blur}
                    />
                )}
                {props.ariaDescription}
            </>
        );
    }
}
