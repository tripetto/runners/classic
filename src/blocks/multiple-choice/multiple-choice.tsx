import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { MultipleChoice } from "@tripetto/block-multiple-choice/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { BlockTitle } from "@ui/blocks/title";
import { BlockCaption } from "@ui/blocks/caption";
import { MultipleChoiceFabric } from "@tripetto/runner-fabric/components/multiple-choice";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-multiple-choice",
    alias: "multiple-choice",
    autoRender: true,
})
export class MultipleChoiceBlock extends MultipleChoice implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.caption ? (
                    <>
                        {props.name && <BlockTitle>{props.name}</BlockTitle>}
                        <BlockCaption>{props.markdownifyToJSX(this.props.caption)}</BlockCaption>
                    </>
                ) : (
                    props.name
                )}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <MultipleChoiceFabric
                    styles={props.styles.multipleChoice}
                    view={props.view}
                    buttons={this.choices(props)}
                    alignment={
                        (this.props.alignment === "equal" || this.props.alignment === "full" || this.props.alignment === "columns"
                            ? this.props.alignment
                            : this.props.alignment && "horizontal") || "vertical"
                    }
                    value={(!this.props.multiple && this.valueOf("choice")) || undefined}
                    required={this.required}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
