import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Checkbox } from "@tripetto/block-checkbox/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { CheckboxFabric } from "@tripetto/runner-fabric/components/checkbox";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-checkbox",
    alias: "checkbox",
})
export class CheckboxBlock extends Checkbox implements IClassicRendering {
    readonly hideRequiredIndicatorFromName = true;

    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.placeholder && props.name}
                {props.description}
                <CheckboxFabric
                    styles={props.styles.checkboxes}
                    value={this.checkboxSlot}
                    required={this.required}
                    error={props.isFailed}
                    label={props.label}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
