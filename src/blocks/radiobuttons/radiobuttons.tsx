import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Radiobuttons } from "@tripetto/block-radiobuttons/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { RadiobuttonsFabric } from "@tripetto/runner-fabric/components/radiobuttons";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-radiobuttons",
})
export class RadiobuttonsBlock extends Radiobuttons implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <RadiobuttonsFabric
                    styles={props.styles.radiobuttons}
                    view={props.view}
                    buttons={this.buttons(props)}
                    value={this.radioSlot}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    allowUnselect={false}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
