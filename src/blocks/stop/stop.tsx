import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Stop } from "@tripetto/block-stop/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockTitle } from "@ui/blocks/title";
import { BlockImage } from "@ui/blocks/image";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-stop",
})
export class StopBlock extends Stop implements IClassicRendering {
    readonly marginAroundBlock = true;

    render(props: IClassicRenderProps): ReactNode {
        return (
            (props.name || props.description || this.props.imageURL) && (
                <>
                    {this.props.imageURL && this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                    {props.name && <BlockTitle>{props.name}</BlockTitle>}
                    {props.description}
                    {this.props.imageURL && !this.props.imageAboveText && (
                        <BlockImage
                            src={props.markdownifyToImage(this.props.imageURL)}
                            width={this.props.imageWidth}
                            isPage={props.isPage}
                        />
                    )}
                </>
            )
        );
    }
}
