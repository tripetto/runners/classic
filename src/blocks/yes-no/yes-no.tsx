import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { YesNo } from "@tripetto/block-yes-no/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { YesNoFabric } from "@tripetto/runner-fabric/components/yes-no";
import { BlockImage } from "@ui/blocks/image";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-yes-no",
    alias: "yes-no",
})
export class YesNoBlock extends YesNo implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToURL(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToURL(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <YesNoFabric
                    styles={props.styles.yesNo}
                    value={this.answerSlot}
                    yes={{
                        label: props.markdownifyToString(this.props.altYes || "") || props.l10n.pgettext("runner#6|🔷 Yes/No", "Yes"),
                        color: this.props.colorYes,
                    }}
                    no={{
                        label: props.markdownifyToString(this.props.altNo || "") || props.l10n.pgettext("runner#6|🔷 Yes/No", "No"),
                        color: this.props.colorNo,
                    }}
                    required={this.required}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
