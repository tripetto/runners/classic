import { styled } from "styled-components";
import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Error } from "@tripetto/block-error/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockTitle } from "@ui/blocks/title";

const ErrorElement = styled.div<{
    $color: string;
}>`
    color: ${(props) => props.$color};

    > h2 {
        font-size: 1.6em;
        line-height: 1.3em;
    }
`;

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-error",
})
export class ErrorBlock extends Error implements IClassicRendering {
    readonly marginAroundBlock = true;

    render(props: IClassicRenderProps): ReactNode {
        return (
            (props.name || props.description) && (
                <ErrorElement $color={props.styles.inputs.errorColor}>
                    {props.name && <BlockTitle>{props.name}</BlockTitle>}
                    {props.description}
                </ErrorElement>
            )
        );
    }
}
