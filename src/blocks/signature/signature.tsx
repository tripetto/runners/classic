import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Signature } from "@tripetto/block-signature/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { SignatureFabric } from "@tripetto/runner-fabric/components/signature";

@tripetto({
    namespace,
    type: "node",
    identifier: "@tripetto/block-signature",
})
export class SignatureBlock extends Signature implements IClassicRendering {
    render(props: IClassicRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <SignatureFabric
                    styles={props.styles.fileUpload}
                    l10n={props.l10n}
                    controller={this}
                    service={props.attachments}
                    error={props.isFailed}
                    size={this.size}
                    color={this.color}
                    tabIndex={props.tabIndex}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                    labels={(id, message) => {
                        switch (id) {
                            case "processing":
                                return props.l10n.pgettext("runner#6|✍️ Signature", "Processing signature (%1)", message);
                            case "signed":
                                return props.l10n.pgettext("runner#6|✍️ Signature", "Signed on %1", message);
                            case "clear":
                                return props.l10n.pgettext("runner#6|✍️ Signature", "Clear");
                        }
                    }}
                />
                {props.ariaDescription}
            </>
        );
    }
}
