import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Dropdown } from "@tripetto/block-dropdown/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { DropdownFabric } from "@tripetto/runner-fabric/components/dropdown";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-dropdown",
})
export class DropdownBlock extends Dropdown implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <DropdownFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    options={this.options}
                    value={this.dropdownSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
