import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { URL } from "@tripetto/block-url/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { URLFabric } from "@tripetto/runner-fabric/components/url";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-url",
})
export class URLBlock extends URL implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <URLFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.urlSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
