import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { PictureChoice } from "@tripetto/block-picture-choice/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockImage } from "@ui/blocks/image";
import { BlockTitle } from "@ui/blocks/title";
import { BlockCaption } from "@ui/blocks/caption";
import { PictureChoiceFabric } from "@tripetto/runner-fabric/components/picture-choice";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-picture-choice",
    autoRender: true,
})
export class PictureChoiceBlock extends PictureChoice implements IClassicRendering {
    readonly hideButtons = !this.props.multiple;
    readonly autoSubmit = !this.props.multiple;

    render(props: IClassicRenderProps, done?: () => void, cancel?: () => void): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.caption ? (
                    <>
                        {props.name && <BlockTitle>{props.name}</BlockTitle>}
                        <BlockCaption>{props.markdownifyToJSX(this.props.caption)}</BlockCaption>
                    </>
                ) : (
                    props.name
                )}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                <PictureChoiceFabric
                    styles={props.styles.pictureChoice}
                    view={props.view}
                    options={this.choices(props)}
                    size={this.props.size}
                    value={(!this.props.multiple && this.valueOf("choice")) || undefined}
                    required={this.required}
                    ariaDescribedBy={props.ariaDescribedBy}
                    autoSubmit={this.autoSubmit}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={done}
                    onCancel={cancel}
                />
                {props.ariaDescription}
            </>
        );
    }
}
