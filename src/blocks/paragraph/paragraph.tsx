import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Paragraph } from "@tripetto/block-paragraph/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { BlockTitle } from "@ui/blocks/title";
import { BlockCaption } from "@ui/blocks/caption";
import { BlockImage } from "@ui/blocks/image";
import { BlockVideo } from "@ui/blocks/video";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-paragraph",
    alias: "paragraph",
})
export class ParagraphBlock extends Paragraph implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {this.props.imageURL && this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {props.name && <BlockTitle>{props.name}</BlockTitle>}
                {this.props.caption && <BlockCaption>{props.markdownifyToJSX(this.props.caption)}</BlockCaption>}
                {props.description}
                {this.props.imageURL && !this.props.imageAboveText && (
                    <BlockImage src={props.markdownifyToImage(this.props.imageURL)} width={this.props.imageWidth} isPage={props.isPage} />
                )}
                {this.props.video && <BlockVideo src={props.markdownifyToURL(this.props.video)} view={props.view} />}
                {props.ariaDescription}
            </>
        );
    }
}
