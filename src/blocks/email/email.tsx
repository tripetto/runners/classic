import { ReactNode } from "react";
import { tripetto } from "@tripetto/runner";
import { namespace } from "@namespace";
import { Email } from "@tripetto/block-email/runner";
import { IClassicRenderProps, IClassicRendering } from "@interfaces/block";
import { EmailFabric } from "@tripetto/runner-fabric/components/email";

@tripetto({
    namespace,
    legacyBlock: true,
    type: "node",
    identifier: "@tripetto/block-email",
})
export class EmailBlock extends Email implements IClassicRendering {
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                {props.name}
                {props.description}
                <EmailFabric
                    id={props.id}
                    styles={props.styles.inputs}
                    value={this.emailSlot}
                    required={this.required}
                    error={props.isFailed}
                    tabIndex={props.tabIndex}
                    placeholder={props.placeholder}
                    ariaDescribedBy={props.ariaDescribedBy}
                    onAutoFocus={props.autoFocus}
                    onFocus={props.focus}
                    onBlur={props.blur}
                    onSubmit={props.onSubmit}
                />
                {props.ariaDescription}
            </>
        );
    }
}
