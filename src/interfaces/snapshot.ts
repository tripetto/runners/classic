export interface IClassicSnapshot {
    /** Contains focus information. */
    readonly b?: {
        [key: string]: false | true | undefined;
    };

    /** Contains the email address that was used to pause. */
    readonly d?: string;

    /** Contains a timestamp of the snapshot. */
    readonly e?: number;
}
