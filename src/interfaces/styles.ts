import { TStyles } from "@tripetto/runner";

export interface IClassicStyles extends TStyles {
    readonly mode?: "paginated" | "continuous" | "progressive";
    readonly color?: string;
    readonly autoFocus?: boolean;
    readonly noBranding?: boolean;
    readonly showEnumerators?: boolean;
    readonly hideRequiredIndicator?: boolean;
    readonly showPageIndicators?: boolean;
    readonly showProgressbar?: boolean;

    readonly font?: {
        readonly family?: string;
        readonly size?: number;
        readonly sizeSmall?: number;
    };

    readonly background?: {
        readonly color?: string;
        readonly url?: string;
        readonly opacity?: number;
        readonly positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
    };

    readonly inputs?: {
        readonly backgroundColor?: string;
        readonly borderColor?: string;
        readonly borderSize?: number;
        readonly roundness?: number;
        readonly textColor?: string;
        readonly errorColor?: string;
        readonly agreeColor?: string;
        readonly declineColor?: string;
        readonly selectionColor?: string;
    };

    readonly buttons?: {
        readonly baseColor?: string;
        readonly textColor?: string;
        readonly roundness?: number;
        readonly mode?: "fill" | "outline";
        readonly finishColor?: string;
    };
}
