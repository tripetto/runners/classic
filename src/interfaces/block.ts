import { L10n, NodeBlock } from "@tripetto/runner";
import { FocusEvent, ReactNode } from "react";
import { IRuntimeStyles } from "@hooks/styles";
import { IRunnerAttachments, TRunnerViews } from "@tripetto/runner-react-hook";
import { TOverlayContext } from "@tripetto/runner-fabric/overlay";

export interface IClassicRenderProps {
    readonly id: string;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly overlay: TOverlayContext;
    readonly view: TRunnerViews;
    readonly name: JSX.Element | undefined;
    readonly description: JSX.Element | undefined;
    readonly explanation: JSX.Element | undefined;
    readonly label: JSX.Element | undefined;
    readonly placeholder: string;
    readonly tabIndex: number;
    readonly isFailed: boolean;
    readonly isPage: boolean;
    readonly ariaDescribedBy: string | undefined;
    readonly ariaDescription: JSX.Element | undefined;
    readonly focus: <T>(e: FocusEvent) => T;
    readonly blur: <T>(e: FocusEvent) => T;
    readonly autoFocus: (element: HTMLElement | null) => void;
    readonly onSubmit: (() => void) | undefined;
    readonly attachments: IRunnerAttachments | undefined;
    readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => JSX.Element;
    readonly markdownifyToURL: (md: string) => string;
    readonly markdownifyToImage: (md: string) => string;
    readonly markdownifyToString: (md: string) => string;
}

export interface IClassicRendering extends NodeBlock {
    readonly required?: boolean;
    readonly marginAroundBlock?: boolean;
    readonly hideRequiredIndicatorFromName?: boolean;
    readonly render?: (props: IClassicRenderProps) => ReactNode;
}
