import { IRunnerProps, TRunnerViews } from "@tripetto/runner-react-hook";
import { IDefinition, ISnapshot, L10n, TL10n } from "@tripetto/runner";
import { CSSProperties, MutableRefObject } from "react";
import { IClassicController } from "@hooks/controller";
import { IClassicSnapshot } from "./snapshot";
import { IClassicStyles } from "./styles";
import { IBuilderInstance } from "@interfaces/builder";

export type TClassicDisplay = "inline" | "page";

export type TClassicPause =
    | {
          readonly recipe: "email";
          readonly onPause: (
              emailAddress: string,
              snapshot: ISnapshot,
              language: string,
              locale: string,
              namespace: string
          ) => Promise<void> | boolean | void;
      }
    | ((snapshot: ISnapshot, language: string, locale: string, namespace: string) => Promise<void> | boolean | void);

export interface IClassicUIProps extends IRunnerProps<IClassicSnapshot> {
    /** Specifies the styles (colors, font, size, etc.) for the runner. */
    readonly styles?: IClassicStyles;

    /** Specifies the localization (locale and translation) information. */
    readonly l10n?: TL10n;

    /** Specifies the initial view mode of the runner. */
    readonly view?: TRunnerViews;

    /** Specifies the display mode of the runner. */
    readonly display?: TClassicDisplay;

    /** Specifies a license code for the runner. */
    readonly license?: string;

    /** Removes all Tripetto branding when a valid license is supplied. */
    readonly removeBranding?: boolean;

    /** Specifies a custom class name for the HTML element that holds the runner. */
    readonly className?: string;

    /** Specifies the inline style for the HTML element that holds the runner. */
    readonly customStyle?: CSSProperties;

    /**
     * Specifies custom CSS rules.
     * To specify rules for a specific block, use this selector: [data-block="<block identifier>"] { ... }
     */
    readonly customCSS?: string;

    /** Specifies a function that is invoked when the runner needs a locale or translation. */
    readonly onL10n?: (l10n: TL10n) => Promise<void>;

    /** Specifies a function that is invoked when the runner wants to reload the definition. */
    readonly onReload?: () => IDefinition | Promise<IDefinition>;

    /** Specifies a function that is invoked when an edit action is requested. */
    readonly onEdit?: (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => void;

    /** Specifies a function or recipe that is invoked when the runner wants to pause. */
    readonly onPause?: TClassicPause;

    /** Specifies a function that is invoked when the runner is "touched" by a user. */
    readonly onTouch?: () => void;

    /** Reference to the runner controller. */
    readonly controller?: MutableRefObject<IClassicController | undefined>;

    /** Specifies a function that is invoked when the runner controller is available. */
    readonly onController?: (controller: MutableRefObject<IClassicController>) => void;
}

export type IClassicProps = Omit<
    IClassicUIProps,
    "definition" | "snapshot" | "styles" | "license" | "l10n" | "onL10n" | "l10nNamespace"
> & {
    /** Specifies the definition to run. */
    readonly definition?: IDefinition | Promise<IDefinition | undefined>;

    /** Specifies the snapshot that should be restored. */
    readonly snapshot?: ISnapshot<IClassicSnapshot> | Promise<ISnapshot<IClassicSnapshot> | undefined>;

    /** Specifies the styles. */
    readonly styles?: IClassicStyles | Promise<IClassicStyles | undefined>;

    /** Specifies a license code for the runner. */
    readonly license?: string | Promise<string | undefined>;

    /** Removes all Tripetto branding when a valid license is supplied. */
    readonly removeBranding?: boolean;

    /** Try to store sessions in the local store to preserve persistency on navigation between multiple pages that host the runner. */
    readonly persistent?: boolean;

    /** Specifies the localization information. */
    readonly l10n?: TL10n | Promise<TL10n | undefined>;

    /** Specifies the preferred language (when no language is specified in the definition). */
    readonly language?: string;

    /** Provides locale information. */
    readonly locale?: L10n.ILocale | ((locale: string) => L10n.ILocale | Promise<L10n.ILocale | undefined> | undefined);

    /** Provides translations. */
    readonly translations?:
        | L10n.TTranslation
        | L10n.TTranslation[]
        | ((
              language: string,
              name: string,
              version: string
          ) => L10n.TTranslation | L10n.TTranslation[] | Promise<L10n.TTranslation | L10n.TTranslation[] | undefined> | undefined);

    /** Specifies a loader that is shown when the runner is loading. */
    readonly loader?: JSX.Element;

    /** Reference to a builder instance to enable live preview for the builder. */
    readonly builder?: MutableRefObject<IBuilderInstance | undefined>;
};
