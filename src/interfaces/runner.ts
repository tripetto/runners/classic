import { IClassicController } from "@hooks/controller";

export interface IClassicRunner extends IClassicController {
    readonly destroy: () => void;
}
