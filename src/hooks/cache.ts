import { ReactNode, useRef } from "react";
import { IObservableNode, TStatus, each } from "@tripetto/runner";
import { TRunnerStatus } from "@tripetto/runner-react-hook";
import { IClassicRendering } from "@interfaces/block";

export interface ICache {
    readonly fetch: (
        create: () => ReactNode,
        node: IObservableNode<IClassicRendering>,
        status: TRunnerStatus | TStatus | "reloading",
        tabIndex: number
    ) => ReactNode;

    readonly purge: () => void;
}

export function useCache(): ICache {
    const cacheRef = useRef<ICache>();

    if (!cacheRef.current) {
        const cache: {
            [node: string]:
                | {
                      readonly buffer: ReactNode;
                      readonly status: TRunnerStatus | TStatus | "reloading";
                      readonly isPassed: boolean;
                      readonly tabIndex: number;
                  }
                | undefined;
        } = {};

        cacheRef.current = {
            fetch: (
                create: () => ReactNode,
                node: IObservableNode<IClassicRendering>,
                status: TRunnerStatus | TStatus | "reloading",
                tabIndex: number
            ) => {
                const key = node.key;
                const current = !node.hasChanged() && cache[key];

                if (current && current.status === status && current.isPassed === node.isPassed && current.tabIndex === tabIndex) {
                    return current.buffer;
                }

                return (cache[key] = {
                    buffer: create(),
                    status,
                    isPassed: node.isPassed,
                    tabIndex,
                }).buffer;
            },
            purge: () => {
                each(
                    cache,
                    (node, key: string) => {
                        delete cache[key];
                    },
                    {
                        keys: true,
                    }
                );
            },
        };
    }

    return cacheRef.current;
}
