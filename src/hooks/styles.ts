import { IClassicStyles } from "@interfaces/styles";
import { useRef } from "react";
import { color } from "@tripetto/runner-fabric/color";
import { Num, TModes, castToNumber } from "@tripetto/runner";
import { SIZE } from "@ui/const";
import cssesc from "cssesc";

export interface IRuntimeStyles extends IClassicStyles {
    readonly font: {
        readonly family: string;
        readonly size: number;
        readonly sizeSmall: number;
        readonly color: string;
    };

    readonly background: {
        readonly color: string;
        readonly opacity: number;
        readonly url?: string;
        readonly positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
    };

    readonly inputs: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly roundness: number | undefined;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
    };

    readonly checkboxes: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly roundness: number | undefined;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
        readonly hideRequiredIndicator: boolean;
    };

    readonly radiobuttons: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly textColor: string;
        readonly scale: number;
    };

    readonly matrix: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
        readonly hideRequiredIndicator: boolean;
    };

    readonly yesNo: {
        readonly yesColor: string;
        readonly noColor: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
        readonly alignment: "horizontal" | "vertical";
    };

    readonly rating: {
        readonly color: string;
        readonly scale: number;
    };

    readonly scale: {
        readonly color: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
        readonly labelColor: string;
    };

    readonly multipleChoice: {
        readonly color: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
    };

    readonly pictureChoice: {
        readonly color: string;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly scale: number;
        readonly margin: number;
    };

    readonly fileUpload: {
        readonly backgroundColor: string;
        readonly borderColor: string;
        readonly borderSize: number;
        readonly roundness: number | undefined;
        readonly textColor: string;
        readonly errorColor: string;
        readonly scale: number;
    };

    readonly buttons: {
        readonly baseColor: string;
        readonly textColor: string | undefined;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly mode: "fill" | "outline";
        readonly scale: number;
    };

    readonly finishButton: {
        readonly baseColor: string;
        readonly textColor: string | undefined;
        readonly outlineSize: number;
        readonly roundness: number | undefined;
        readonly mode: "fill" | "outline";
        readonly scale: number;
    };
}

const generateRuntimeStyles = (styles: IClassicStyles, hasLicense: boolean, removeBranding: boolean): IRuntimeStyles => {
    const backgroundColor = cssesc((styles.background && styles.background.color) || "transparent");
    const textColor = cssesc(
        color(styles.color || color(backgroundColor, (o) => o.makeUnclear("#fff").makeBlackOrWhite()), (o) => o.makeUnclear("#000"))
    );

    return {
        ...styles,
        ...{
            background: {
                color: backgroundColor,
                url:
                    (styles.background &&
                        styles.background.url &&
                        cssesc(styles.background.url, {
                            quotes: "double",
                        })) ||
                    undefined,
                opacity:
                    styles.background?.color && styles.background?.url
                        ? Num.range(castToNumber(styles.background?.opacity, 100) * 0.01, 0, 1)
                        : 1,
                positioning: cssesc((styles.background && styles.background.positioning) || "auto") as
                    | "auto"
                    | "100% auto"
                    | "auto 100%"
                    | "cover"
                    | "contain"
                    | "repeat",
            },
            font: {
                family: cssesc((styles.font && styles.font.family) || "sans-serif"),
                size: (styles.font && styles.font.size && Num.range(castToNumber(styles.font.size, SIZE), 8, 30)) || SIZE,
                sizeSmall: (styles.font && styles.font.sizeSmall && Num.range(castToNumber(styles.font.sizeSmall, SIZE), 8, 30)) || SIZE,
                color: textColor,
            },
            inputs: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                textColor: cssesc((styles.inputs && styles.inputs.textColor) || textColor),
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 0.6075 / 0.375,
            },
            checkboxes: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                textColor,
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 1,
                hideRequiredIndicator: styles.hideRequiredIndicator || false,
            },
            radiobuttons: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                textColor,
                scale: 1,
            },
            matrix: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                textColor,
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 1,
                hideRequiredIndicator: styles.hideRequiredIndicator || false,
            },
            yesNo: {
                yesColor: cssesc((styles.inputs && styles.inputs.agreeColor) || "green"),
                noColor: cssesc((styles.inputs && styles.inputs.declineColor) || "red"),
                alignment: "horizontal",
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
            },
            rating: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                scale: 2,
            },
            scale: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
                labelColor: textColor,
            },
            multipleChoice: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
            },
            pictureChoice: {
                color: cssesc((styles.inputs && styles.inputs.selectionColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                scale: 0.6075 / 0.375,
                margin: 8,
            },
            fileUpload: {
                backgroundColor: cssesc((styles.inputs && styles.inputs.backgroundColor) || "transparent"),
                borderColor: cssesc((styles.inputs && styles.inputs.borderColor) || textColor),
                borderSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.inputs?.roundness,
                textColor: cssesc((styles.inputs && styles.inputs.textColor) || textColor),
                errorColor: cssesc((styles.inputs && styles.inputs.errorColor) || "red"),
                scale: 1,
            },
            buttons: {
                baseColor: cssesc((styles.buttons && styles.buttons.baseColor) || textColor),
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                roundness: styles.buttons?.roundness,
                mode: styles.buttons && styles.buttons.mode === "outline" ? "outline" : "fill",
                textColor: (styles.buttons && styles.buttons.textColor && cssesc(styles.buttons.textColor)) || undefined,
                scale: 0.6075 / 0.375,
            },
            finishButton: {
                baseColor: cssesc((styles.buttons && (styles.buttons.finishColor || styles.buttons.baseColor)) || "green"),
                roundness: styles.buttons?.roundness,
                outlineSize: (styles.inputs && castToNumber(styles.inputs.borderSize, 1)) || 1,
                textColor:
                    (styles.buttons && !styles.buttons.finishColor && styles.buttons.textColor && cssesc(styles.buttons.textColor)) ||
                    undefined,
                mode: "fill",
                scale: 0.6075 / 0.375,
            },
            noBranding: (hasLicense && (removeBranding || styles.noBranding)) || false,
        },
    };
};

export const useStyles = (
    initialStyles: IClassicStyles | undefined,
    hasLicense: boolean,
    removeBranding: boolean,
    onMode: (mode: TModes) => void,
    onChange: () => void
) => {
    const designtimeStyles = useRef<IClassicStyles>(initialStyles || {});
    const runtimeStyles = useRef<IRuntimeStyles>();

    if (!runtimeStyles.current) {
        runtimeStyles.current = generateRuntimeStyles(designtimeStyles.current, hasLicense, removeBranding);
    }

    return [
        designtimeStyles.current,
        runtimeStyles.current,
        (styles: IClassicStyles) => {
            designtimeStyles.current = styles;
            runtimeStyles.current = generateRuntimeStyles(styles, hasLicense, removeBranding);

            onMode(styles.mode === "paginated" ? "paginated" : styles.mode === "continuous" ? "continuous" : "progressive");
            onChange();
        },
    ] as [IClassicStyles, IRuntimeStyles, (styles: IClassicStyles) => void];
};
