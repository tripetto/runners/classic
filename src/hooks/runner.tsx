import { useRef } from "react";
import {
    DateTime,
    IObservableNode,
    castToBoolean,
    each,
    findFirst,
    isString,
    markdownifyToString,
    markdownifyToURL,
} from "@tripetto/runner";
import { markdownifyToJSX } from "@tripetto/runner-react-hook";
import { RequiredIndicatorFabric } from "@tripetto/runner-fabric/components/required-indicator";
import { warningIcon } from "@tripetto/runner-fabric/icons/warning";
import { IClassicUIProps } from "@interfaces/props";
import { Page } from "@ui/pages";
import { Block, Blocks } from "@ui/blocks";
import { BlockTitle } from "@ui/blocks/title";
import { BlockName } from "@ui/blocks/name";
import { BlockDescription } from "@ui/blocks/description";
import { BlockExplanation } from "@ui/blocks/explanation";
import { useClassicController } from "./controller";
import { useOverlay } from "@tripetto/runner-fabric/overlay";
import { useFocus } from "./focus";
import { IClassicRendering } from "@interfaces/block";

export const useClassicRunner = (props: IClassicUIProps) => {
    const [runner, cache, l10n, styles, attachments, doAction] = useClassicController({
        ...props,
        onSnapshot: () => ({
            b,
            d: props.snapshot && props.snapshot.b && props.snapshot.b.d,
            e: DateTime.now,
        }),
        onRestart: () => blur(),
        onScrollIntoView: (id) => {
            const node = findFirst(runner.storyline?.all, (n) => n.node.id === id);

            if (node) {
                const element = blocksRef.current[node.key];

                if (element) {
                    element.scrollIntoView({
                        behavior: "smooth",
                        block: "center",
                        inline: "center",
                    });

                    return true;
                }
            }

            return false;
        },
    });

    const [OverlayProvider, overlay] = useOverlay();
    const [frameRef, blur, hasOrHadFocus, handleFocus, handleAutoFocus, b] = useFocus({
        page: runner.page,
        gainFocus: runner.view === "live" && (props.display === "page" || styles.autoFocus),
        initialFocus: props.snapshot && props.snapshot.b && props.snapshot.b.b,
    });

    const blocksRef = useRef<{
        [key: string]: HTMLElement | undefined;
    }>({});

    const tabRef = useRef<
        {
            node: string;
            first: number;
            last: number;
        }[]
    >([]);

    const stagedRef = useRef<{
        [key: string]: IObservableNode<IClassicRendering> | undefined;
    }>({});

    const mode = runner.storyline?.mode || "progressive";
    const status = runner.status;
    const view = runner.view;
    const isPage = view !== "live" || props.display === "page";
    const isEvaluating = (runner.storyline && view !== "preview" && runner.storyline.isEvaluating) || false;
    const staged: {
        [key: string]: IObservableNode<IClassicRendering>;
    } = {};
    let submitCount = 0;
    let total = 0;
    const allowSubmit =
        runner.storyline &&
        !runner.storyline.isFailed &&
        !runner.storyline.isAtFinish &&
        !runner.storyline.isFinishable &&
        !findFirst(runner.storyline.all, (n) => {
            if (n.node.slots.count > 0) {
                submitCount++;
            }

            return submitCount > 1;
        }) &&
        submitCount === 1;

    const blocks = runner.preview !== "epilogue" && runner.storyline && !runner.storyline.isEmpty && (
        <Blocks $styles={styles} $view={view}>
            {runner.storyline.map((moment, page) => (
                <Page key={page} page={page} view={view} l10n={l10n} styles={styles} title={moment.section.props.name}>
                    {moment.nodes.map((node) => {
                        const index = total++;
                        const previousTab = (index > 0 && index - 1 < tabRef.current.length && tabRef.current[index - 1]) || undefined;
                        let currentTab = (index < tabRef.current.length && tabRef.current[index]) || undefined;

                        if (!currentTab || currentTab.node !== node.key || (previousTab && previousTab.last !== currentTab.first)) {
                            currentTab = {
                                node: node.key,
                                first: previousTab?.last || 0,
                                last: previousTab?.last || 0,
                            };

                            tabRef.current.splice(index, tabRef.current.length - index, currentTab);
                        }

                        staged[node.key] = node;

                        return cache.fetch(
                            () => {
                                currentTab!.last = currentTab!.first;

                                const fnEdit =
                                    (props.onEdit && view !== "live" && node.id && (() => props.onEdit!("block", node.id))) || undefined;
                                const block =
                                    node.block?.render &&
                                    node.block?.render({
                                        l10n,
                                        styles,
                                        overlay,
                                        view,
                                        attachments,
                                        get id() {
                                            return node.block?.key() || "";
                                        },
                                        get name() {
                                            return (
                                                (node.props.name && castToBoolean(node.props.nameVisible, true) && (
                                                    <BlockName
                                                        htmlFor={node.block?.key()}
                                                        onClick={fnEdit}
                                                        $hasDescription={node.props.description ? true : false}
                                                    >
                                                        {styles.showEnumerators && node.enumerator && `${node.enumerator}. `}
                                                        {markdownifyToJSX(node.props.name, node.context)}
                                                        {!styles.hideRequiredIndicator &&
                                                            node.block?.required &&
                                                            !node.block.hideRequiredIndicatorFromName && (
                                                                <RequiredIndicatorFabric $errorColor={styles.inputs.errorColor} />
                                                            )}
                                                    </BlockName>
                                                )) ||
                                                undefined
                                            );
                                        },
                                        get description() {
                                            return (
                                                (node.props.description && (
                                                    <BlockDescription onClick={fnEdit}>
                                                        {markdownifyToJSX(node.props.description, node.context)}
                                                    </BlockDescription>
                                                )) ||
                                                undefined
                                            );
                                        },
                                        get explanation() {
                                            return (
                                                (node.props.explanation && markdownifyToJSX(node.props.explanation, node.context)) ||
                                                undefined
                                            );
                                        },
                                        get placeholder() {
                                            return markdownifyToString(node.props.placeholder || "", node.context) || "";
                                        },
                                        get label() {
                                            return markdownifyToJSX(
                                                node.props.placeholder || node.props.name || "...",
                                                node.context,
                                                false
                                            );
                                        },
                                        get tabIndex(): number {
                                            return ++currentTab!.last;
                                        },
                                        get isFailed() {
                                            return (view !== "preview" && node.isFailed && hasOrHadFocus(node) !== undefined) || false;
                                        },
                                        get ariaDescribedBy() {
                                            return (node.props.explanation && node.block?.key("explanation")) || undefined;
                                        },
                                        get ariaDescription() {
                                            return (
                                                (node.props.explanation && (
                                                    <BlockExplanation id={node.block?.key("explanation")} onClick={fnEdit}>
                                                        {markdownifyToJSX(node.props.explanation, node.context)}
                                                    </BlockExplanation>
                                                )) ||
                                                undefined
                                            );
                                        },
                                        isPage,
                                        focus: handleFocus(node, true, () => doAction("focus", node)),
                                        blur: handleFocus(node, false, () => doAction("blur", node)),
                                        autoFocus: handleAutoFocus(node),
                                        markdownifyToJSX: (md: string, lineBreaks?: boolean) =>
                                            markdownifyToJSX(md, node.context, lineBreaks),
                                        markdownifyToURL: (md: string) => markdownifyToURL(md, node.context),
                                        markdownifyToImage: (md: string) =>
                                            markdownifyToURL(md, node.context, undefined, [
                                                "image/jpeg",
                                                "image/png",
                                                "image/svg",
                                                "image/gif",
                                            ]) || `data:image/svg+xml;base64,${warningIcon(styles.color || "#000")}`,
                                        markdownifyToString: (md: string, lineBreaks?: boolean) =>
                                            markdownifyToString(md, node.context, undefined, lineBreaks),
                                        onSubmit:
                                            (allowSubmit &&
                                                (() => {
                                                    if (runner.storyline) {
                                                        let count = 0;

                                                        each(runner.storyline.all, (n) => {
                                                            if (n.node.slots.count > 0) {
                                                                count++;
                                                            }
                                                        });

                                                        if (
                                                            count === 1 &&
                                                            !runner.storyline.isFailed &&
                                                            !(runner.storyline.isAtFinish && !runner.storyline.isFinishable)
                                                        ) {
                                                            runner.storyline.stepForward();
                                                        }
                                                    }
                                                })) ||
                                            undefined,
                                    });

                                return (
                                    <Block
                                        key={node.key}
                                        ref={(el: HTMLDivElement) => {
                                            blocksRef.current[node.key] = el || undefined;
                                        }}
                                        data-block={node.block?.type.identifier || undefined}
                                        $marginAroundBlock={!block || node.block?.marginAroundBlock}
                                        $locked={status === "pausing"}
                                    >
                                        {block || (
                                            <>
                                                {isString(node.props.name) && castToBoolean(node.props.nameVisible, true) && (
                                                    <BlockTitle onClick={fnEdit}>
                                                        {markdownifyToJSX(node.props.name, node.context)}
                                                    </BlockTitle>
                                                )}
                                                {node.props.description && (
                                                    <BlockDescription onClick={fnEdit}>
                                                        {markdownifyToJSX(node.props.description, node.context)}
                                                    </BlockDescription>
                                                )}
                                            </>
                                        )}
                                    </Block>
                                );
                            },
                            node,
                            status,
                            currentTab.first
                        );
                    })}
                </Page>
            ))}
        </Blocks>
    );

    if (props.onAction) {
        each(stagedRef.current, (node) => {
            if (node && !staged[node.key]) {
                stagedRef.current[node.key] = undefined;

                doAction("unstage", node);
            }
        });

        each(staged, (node) => {
            if (!stagedRef.current[node.key]) {
                stagedRef.current[node.key] = node;

                doAction("stage", node);
            }
        });
    }

    return {
        frameRef,
        status,
        view,
        isPage,
        mode,
        title: runner.title,
        l10n,
        styles,
        prologue: runner.prologue && {
            ...runner.prologue,
            l10n,
            styles,
            view,
            isPage,
            start: runner.preview === "prologue" ? runner.resetPreview : runner.start,
            edit: (view !== "live" && props.onEdit && (() => props.onEdit!("prologue"))) || undefined,
        },
        blocks,
        buttons:
            (view !== "preview" &&
                status !== "pausing" &&
                runner.storyline && {
                    previous: (!runner.storyline.isAtStart && (() => runner.storyline!.stepBackward())) || undefined,
                    next:
                        (!runner.storyline.isFailed &&
                            !(runner.storyline.isAtFinish && !runner.storyline.isFinishable) &&
                            (() => runner.storyline!.stepForward())) ||
                        undefined,
                    finish: (runner.storyline.isFinishable && (() => runner.storyline!.finish())) || undefined,
                    finishable: view !== "live" || props.onSubmit || !(runner.storyline && runner.storyline.hasDataCollected),
                    pause: runner.pause,
                    reload: runner.reload,
                }) ||
            undefined,
        epilogue: runner.epilogue && {
            ...runner.epilogue,
            l10n,
            styles,
            view,
            isPage,
            repeat: runner.restart,
            edit: (view !== "live" && props.onEdit && (() => props.onEdit!("epilogue", runner.epilogue?.branch))) || undefined,
        },
        pausing: runner.pausing,
        isEvaluating,
        progressPercentage: runner.storyline?.percentage || 0,
        pages: runner.storyline?.pages || [],
        tabIndex: tabRef.current.length > 0 ? tabRef.current[tabRef.current.length - 1].last : 0,
        OverlayProvider,
    };
};
