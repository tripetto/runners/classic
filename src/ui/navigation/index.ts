import { styled } from "styled-components";

export const Navigation = styled.nav`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    align-items: center;
    justify-content: space-between;
`;
