import { styled } from "styled-components";
import { SIZE } from "@ui/const";

export const Buttons = styled.div`
    margin-top: ${24 / SIZE}em !important;
    margin-right: ${16 / SIZE}em;
    display: flex;

    * + * {
        margin-left: ${16 / SIZE}em;
    }
`;
