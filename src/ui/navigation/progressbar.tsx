import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { color } from "@tripetto/runner-fabric/color";
import { SIZE } from "@ui/const";

const ProgressbarElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    flex-grow: 2;
    margin-top: ${24 / SIZE}em;
    margin-right: ${16 / SIZE}em;
    background-color: ${(props) => color(props.$styles.buttons.baseColor, (o) => o.manipulate((m) => m.alpha(0.2)))};
    border-radius: 0.5em;
`;

const ProgressElement = styled.div<{
    $styles: IRuntimeStyles;
    $percentage: number;
}>`
    background-color: ${(props) => (props.$percentage > 0 && props.$styles.buttons.baseColor) || undefined};
    color: ${(props) =>
        color(props.$styles.buttons.textColor || props.$styles.buttons.baseColor, (o) =>
            o.makeBlackOrWhite(!props.$styles.buttons.textColor)
        )};
    font-size: 0.8em;
    padding: ${(props) =>
        `${0.1 * props.$styles.buttons.scale}em ${0.4 * props.$styles.buttons.scale}em ${0.1 * props.$styles.buttons.scale}em ${
            props.$percentage > 0 ? 0 : 0.4 * props.$styles.buttons.scale
        }em`};
    text-align: ${(props) => (props.$percentage > 0 ? "right" : "left")};
    border-radius: 0.5em;
    white-space: no-wrap;
    overflow: hidden;
    transition: width 0.3s ease;
    width: ${(props) => (props.$percentage > 0 && props.$percentage + "%") || undefined};
`;

export const Progressbar = (props: { readonly percentage: number; readonly styles: IRuntimeStyles }) => (
    <ProgressbarElement $styles={props.styles}>
        <ProgressElement
            role="progressbar"
            aria-valuenow={props.percentage}
            aria-valuemin={0}
            aria-valuemax={100}
            $styles={props.styles}
            $percentage={props.percentage}
        >
            {props.percentage}%
        </ProgressElement>
    </ProgressbarElement>
);
