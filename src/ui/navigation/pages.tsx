import { styled } from "styled-components";
import { IPage } from "@tripetto/runner";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { IRuntimeStyles } from "@hooks/styles";
import { SIZE } from "@ui/const";

const PagesElement = styled.div`
    margin-top: ${24 / SIZE}em;
`;

export const Pages = (props: { readonly pages: IPage[]; readonly disabled?: boolean; readonly styles: IRuntimeStyles }) =>
    props.pages.length > 0 ? (
        <PagesElement>
            {props.pages.map((page, index) => (
                <ButtonFabric
                    key={index}
                    styles={{
                        ...props.styles.buttons,
                        mode: page.active ? "fill" : "outline",
                        group:
                            (props.pages.length > 1 && (index === 0 ? "start" : index + 1 === props.pages.length ? "end" : "middle")) ||
                            undefined,
                    }}
                    label={`${page.number}`}
                    disabled={props.disabled}
                    onClick={page.activate}
                />
            ))}
        </PagesElement>
    ) : (
        <></>
    );
