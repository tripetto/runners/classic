import { styled } from "styled-components";
import { L10n } from "@tripetto/runner";
import { IRuntimeStyles } from "@hooks/styles";
import { SIZE } from "@ui/const";

export const EvaluatingElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    margin-top: ${24 / SIZE}em;
    color: ${(props) => props.$styles.font.color};
`;

export const EvaluatingMessage = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) => (
    <EvaluatingElement $styles={props.styles}>
        {props.l10n.pgettext("runner#3|🩺 Status information", "⏳ One moment please...")}
    </EvaluatingElement>
);
