import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { color } from "@tripetto/runner-fabric/color";
import { SIZE } from "@ui/const";

const BannerElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    display: block;
    color: ${(props) => color(props.$styles.font.color, (o) => o.manipulate((m) => m.alpha(0.4)))};
    margin-top: ${16 / SIZE}em;
    font-size: 0.85em;

    > a {
        text-decoration: none;
        transition: color 0.5s;

        &:hover {
            color: ${(props) => props.$styles.font.color} !important;
            text-decoration: none;
        }
    }
`;

export const Banner = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) =>
    !props.styles.noBranding ? (
        <BannerElement $styles={props.styles}>
            <a
                href="https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_classic&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=form"
                target="_blank"
            >
                {props.l10n.pgettext("runner:classic", "Powered by Tripetto")}
            </a>
        </BannerElement>
    ) : (
        <></>
    );
