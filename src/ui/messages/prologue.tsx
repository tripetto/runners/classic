import { IPrologue, L10n } from "@tripetto/runner";
import { TRunnerViews, markdownifyToJSX } from "@tripetto/runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { Block, Blocks } from "@ui/blocks";
import { BlockImage } from "@ui/blocks/image";
import { BlockTitle } from "@ui/blocks/title";
import { BlockDescription } from "@ui/blocks/description";
import { BlockVideo } from "@ui/blocks/video";
import { BlockButtons } from "@ui/blocks/buttons";
import { NextIcon } from "@ui/icons/next";
import { Banner } from "@ui/messages/banner";

export const Prologue = (
    props: IPrologue & {
        readonly l10n: L10n.Namespace;
        readonly styles: IRuntimeStyles;
        readonly view: TRunnerViews;
        readonly isPage: boolean;
        readonly start: () => void;
        readonly edit?: () => void;
    }
) => (
    <Blocks $styles={props.styles} $view={props.view} $center={props.isPage}>
        <Block>
            {props.image && (
                <BlockImage src={props.image} alignment={props.isPage ? "center" : "left"} isPage={props.isPage} onClick={props.edit} />
            )}
            {props.title && (
                <BlockTitle onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                    {markdownifyToJSX(props.title)}
                </BlockTitle>
            )}
            {props.description && (
                <BlockDescription onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                    {markdownifyToJSX(props.description)}
                </BlockDescription>
            )}
            {props.video && <BlockVideo src={props.video} view={props.view} />}
            <BlockButtons>
                <ButtonFabric
                    styles={props.styles.buttons}
                    icon={NextIcon}
                    label={props.button || props.l10n.pgettext("runner#1|🆗 Buttons", "Start")}
                    onClick={props.start}
                    onAutoFocus={(e) => {
                        if (props.view === "live" && (props.styles.autoFocus || props.isPage) && e) {
                            e.focus();
                        }
                    }}
                />
            </BlockButtons>
            {props.view !== "preview" && <Banner l10n={props.l10n} styles={props.styles} />}
        </Block>
    </Blocks>
);
