import { IEpilogue, L10n, isBoolean, markdownifyToString, markdownifyToURL } from "@tripetto/runner";
import { TRunnerViews, markdownifyToJSX } from "@tripetto/runner-react-hook";
import { IRuntimeStyles } from "@hooks/styles";
import { Block, Blocks } from "@ui/blocks";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { BlockImage } from "@ui/blocks/image";
import { BlockTitle } from "@ui/blocks/title";
import { BlockCaption } from "@ui/blocks/caption";
import { BlockDescription } from "@ui/blocks/description";
import { BlockVideo } from "@ui/blocks/video";
import { BlockButtons } from "@ui/blocks/buttons";
import { RestartIcon } from "@ui/icons/restart";
import { EditIcon } from "@ui/icons/edit";

export const Epilogue = (
    props: IEpilogue & {
        readonly l10n: L10n.Namespace;
        readonly styles: IRuntimeStyles;
        readonly view: TRunnerViews;
        readonly isPage: boolean;
        readonly repeat?: () => void;
        readonly edit?: () => void;
    }
) => {
    if (props.view === "live" && props.getRedirect && props.getRedirect()) {
        return <></>;
    }

    return (
        <Blocks $styles={props.styles} $view={props.view} $center={props.isPage}>
            <Block>
                {props.view !== "live" && props.redirectUrl ? (
                    <>
                        <BlockTitle onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                            {props.view === "preview"
                                ? props.l10n.pgettext("runner:classic", "🌍 Redirect preview")
                                : props.l10n.pgettext("runner:classic", "🎉 Test completed")}
                        </BlockTitle>
                        <BlockDescription onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                            {props.l10n.pgettext("runner:classic", "In a live environment the form will redirect to:")}
                            <br />
                            <a href={markdownifyToURL(props.redirectUrl, props.context) || "#"} target="_blank">
                                {markdownifyToURL(props.redirectUrl, props.context) ||
                                    props.l10n.pgettext("runner:classic", "The supplied URL is invalid!")}
                            </a>
                        </BlockDescription>
                        {props.view === "test" && (
                            <BlockButtons $alignment={props.isPage ? "center" : "left"}>
                                <ButtonFabric
                                    styles={{ ...props.styles.buttons, mode: "outline" }}
                                    label={props.l10n.pgettext("runner:classic", "Test again")}
                                    icon={RestartIcon}
                                    disabled={!props.repeat}
                                    onClick={props.repeat}
                                />
                            </BlockButtons>
                        )}
                    </>
                ) : (
                    <>
                        {props.image && (
                            <BlockImage
                                src={markdownifyToURL(props.image, props.context, undefined, [
                                    "image/jpeg",
                                    "image/png",
                                    "image/svg",
                                    "image/gif",
                                ])}
                                alignment="center"
                                isPage={props.isPage}
                                onClick={props.edit}
                            />
                        )}
                        <BlockTitle onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                            {markdownifyToJSX(
                                props.title ||
                                    (props.view === "test" && props.l10n.pgettext("runner:classic", "🎉 Test completed")) ||
                                    props.l10n.pgettext("runner#2|💬 Messages|Conversation ended", "🎉 It's a wrap!"),
                                props.context
                            )}
                        </BlockTitle>
                        {props.description ? (
                            <BlockDescription onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                                {markdownifyToJSX(props.description, props.context)}
                            </BlockDescription>
                        ) : (
                            !props.title &&
                            props.view === "test" && (
                                <BlockDescription onClick={props.edit} $alignment={props.isPage ? "center" : "left"}>
                                    {props.l10n.pgettext("runner:classic", "Did you know you can customize this closing message?")}
                                </BlockDescription>
                            )
                        )}
                        {props.video && <BlockVideo src={markdownifyToURL(props.video, props.context)} view={props.view} play={true} />}
                        {(props.button || props.repeatable || props.view === "test") && (
                            <BlockButtons $alignment={props.isPage ? "center" : "left"}>
                                {props.button && (
                                    <ButtonFabric
                                        styles={{ ...props.styles.buttons, mode: "fill" }}
                                        label={markdownifyToString(props.button.label, props.context)}
                                        hyperlink={{
                                            url: markdownifyToURL(props.button.url, props.context),
                                            target: props.view === "preview" || props.view === "test" ? "blank" : props.button.target,
                                        }}
                                    />
                                )}
                                {(props.repeatable || props.view === "test") && (
                                    <>
                                        <ButtonFabric
                                            styles={{ ...props.styles.buttons, mode: "outline" }}
                                            label={
                                                props.repeatable
                                                    ? props.l10n.pgettext("runner#1|🆗 Buttons", "Start again")
                                                    : props.l10n.pgettext("runner:classic", "Test again")
                                            }
                                            icon={RestartIcon}
                                            disabled={!props.repeat}
                                            onClick={props.repeat}
                                        />
                                        {props.edit &&
                                            !props.image &&
                                            !props.title &&
                                            !props.description &&
                                            !props.video &&
                                            !props.button &&
                                            !isBoolean(props.repeatable) && (
                                                <ButtonFabric
                                                    styles={{ ...props.styles.buttons, mode: "outline" }}
                                                    label={props.l10n.pgettext("runner:classic", "Customize")}
                                                    icon={EditIcon}
                                                    onClick={props.edit}
                                                />
                                            )}
                                    </>
                                )}
                            </BlockButtons>
                        )}
                    </>
                )}
            </Block>
            {props.view === "live" && !props.styles.noBranding && (
                <Block $marginAroundBlock={true}>
                    <BlockCaption $alignment={props.isPage ? "center" : "left"}>
                        {props.l10n.pgettext("runner:classic", "Want to make a form like this for free?")}
                    </BlockCaption>
                    <BlockDescription $alignment={props.isPage ? "center" : "left"}>
                        {props.l10n.pgettext(
                            "runner:classic",
                            "Tripetto is for making elegantly personal form and survey experiences with response boosting conversational powers."
                        )}
                    </BlockDescription>
                    <BlockButtons $alignment={props.isPage ? "center" : "left"}>
                        <ButtonFabric
                            styles={{ ...props.styles.buttons, mode: "fill" }}
                            label={props.l10n.pgettext("runner:classic", "Create one now!")}
                            hyperlink={{
                                url: "https://tripetto.com/your-tripetto-experience/?utm_source=tripetto_runner_classic&utm_medium=tripetto_runners&utm_campaign=tripetto_branding&utm_content=closing",
                                target: "blank",
                            }}
                        />
                    </BlockButtons>
                </Block>
            )}
        </Blocks>
    );
};
