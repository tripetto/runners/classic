import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { Block, Blocks } from "@ui/blocks";
import { BlockTitle } from "@ui/blocks/title";
import { BlockDescription } from "@ui/blocks/description";

export const PausedMessage = (props: {
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
}) => (
    <Blocks $styles={props.styles} $view={props.view} $center={props.isPage}>
        <Block>
            <BlockTitle $alignment={props.isPage ? "center" : "left"}>
                {props.l10n.pgettext("runner#2|💬 Messages|Conversation paused", "⏸ Paused")}
            </BlockTitle>
            <BlockDescription $alignment={props.isPage ? "center" : "left"}>
                {props.l10n.pgettext("runner#2|💬 Messages|Conversation paused", "The conversation is now paused.")}
            </BlockDescription>
        </Block>
    </Blocks>
);
