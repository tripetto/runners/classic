import { styled } from "styled-components";
import { ReactNode } from "react";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { L10n } from "@tripetto/runner";
import { IClassicStyles } from "@interfaces/styles";
import { SIZE } from "@ui/const";

const PageElement = styled.div<{
    $color: string;
}>`
    margin: ${16 / SIZE}em 0;
    display: flex;
    align-items: center;

    &:first-child {
        margin-top: 0;
    }

    > div {
        flex-grow: 1;

        &:nth-child(2) {
            flex-grow: 0;
            max-width: 80%;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            padding: 0 ${8 / SIZE}em;
            font-size: 11px;
            font-family: Tahoma, Arial;
            text-transform: uppercase;
            color: ${(props) => props.$color};
            opacity: 0.5;
            cursor: default;
        }

        &:first-child,
        &:last-child {
            height: 1px;
            border-bottom: ${(props) => `1px dashed ${props.$color}`};
            opacity: 0.25;
        }
    }
`;

export const Page = (props: {
    readonly page: number;
    readonly view: TRunnerViews;
    readonly l10n: L10n.Namespace;
    readonly styles: IClassicStyles;
    readonly title?: string;
    readonly children?: ReactNode;
}) =>
    props.view === "preview" ? (
        <>
            <PageElement $color={props.styles.color || "#000"}>
                <div></div>
                <div>
                    {(props.styles.mode === "paginated"
                        ? props.l10n.pgettext("runner:classic", "Page %1", L10n.Locales.number(props.page + 1))
                        : props.l10n.pgettext("runner:classic", "Section %1", L10n.Locales.number(props.page + 1))) +
                        ((props.title && ` - ${props.title}`) || "")}
                </div>
                <div></div>
            </PageElement>
            {props.children}
        </>
    ) : (
        <>{props.children}</>
    );
