import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { SIZE } from "@ui/const";

export const ConnectionErrorElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    margin-top: ${24 / SIZE}em;
    border: 1px solid ${(props) => props.$styles.inputs.errorColor};
    color: ${(props) => props.$styles.inputs.errorColor};
    padding: 0.4em;
    border-radius: 0.5em;
`;

export const ConnectionError = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) => (
    <ConnectionErrorElement $styles={props.styles}>
        <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "Something went wrong while submitting your conversation.")}</b>
        <br />
        {props.l10n.pgettext(
            "runner#9|⚠ Errors|Connection error",
            "Please check your connection and try again (for the techies: The error console of your browser might contain more technical information about what went wrong)."
        )}
    </ConnectionErrorElement>
);
