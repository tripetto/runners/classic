import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { SIZE } from "@ui/const";

export const OutdatedErrorElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    margin-top: ${24 / SIZE}em;
    border: 1px solid ${(props) => props.$styles.inputs.errorColor};
    color: ${(props) => props.$styles.inputs.errorColor};
    padding: 0.4em;
    border-radius: 0.5em;
`;

export const OutdatedError = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles; readonly allowReload: boolean }) => (
    <OutdatedErrorElement $styles={props.styles}>
        <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "The form is outdated.")}</b>
        <br />
        {props.allowReload
            ? props.l10n.pgettext(
                  "runner#9|⚠ Errors|Outdated error",
                  "It seems the form is changed by the owner while you were filling in the form. Your data cannot be processed, but you can try to reload the new version of the form while maintaining the data you've filled in. Click the reload button to try this. Please check your data after reloading and then submit again."
              )
            : props.l10n.pgettext(
                  "runner#9|⚠ Errors|Outdated error",
                  "It seems the form is changed by the owner while you were filling in the form. Your data cannot be processed. Try reloading the form."
              )}
    </OutdatedErrorElement>
);
