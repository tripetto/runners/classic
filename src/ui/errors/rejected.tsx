import { styled } from "styled-components";
import { IRuntimeStyles } from "@hooks/styles";
import { L10n } from "@tripetto/runner";
import { SIZE } from "@ui/const";

export const RejectedErrorElement = styled.div<{
    $styles: IRuntimeStyles;
}>`
    margin-top: ${24 / SIZE}em;
    border: 1px solid ${(props) => props.$styles.inputs.errorColor};
    color: ${(props) => props.$styles.inputs.errorColor};
    padding: 0.4em;
    border-radius: 0.5em;
`;

export const RejectedError = (props: { readonly l10n: L10n.Namespace; readonly styles: IRuntimeStyles }) => (
    <RejectedErrorElement $styles={props.styles}>
        <b>{props.l10n.pgettext("runner#9|⚠ Errors|Submit error", "Your data is rejected.")}</b>
        <br />
        {props.l10n.pgettext(
            "runner#9|⚠ Errors|Rejected error",
            "Unfortunately, your data is marked as invalid and therefore rejected. If you believe this is a mistake, please contact the form owner. We're sorry for the inconvenience."
        )}
    </RejectedErrorElement>
);
