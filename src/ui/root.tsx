import { styled, StyleSheetManager, createGlobalStyle, css } from "styled-components";
import { CSSProperties, MutableRefObject, ReactNode, useEffect, useRef, useState } from "react";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { color } from "@tripetto/runner-fabric/color";
import { Num, Str } from "@tripetto/runner";
import { IRuntimeStyles } from "@hooks/styles";
import { Frame, FrameConfig } from "@hooks/frame";
import { attach, detach } from "@helpers/resizer";
import { Blocks } from "@ui/blocks";
import { MARGIN, OFFSET, SIZE, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE } from "@ui/const";

const RootFrame = styled(Frame).withConfig(FrameConfig)<{
    $view: TRunnerViews;
    $isPage: boolean;
    $styles: IRuntimeStyles;
}>`
    background-color: transparent !important;
    border: none !important;
    position: ${(props) => (props.$view === "live" && props.$isPage ? "fixed" : "relative")};
    left: ${(props) => (props.$view === "live" ? (props.$isPage ? 0 : `-${OFFSET}px`) : undefined)};
    top: ${(props) => (props.$view === "live" ? (props.$isPage ? 0 : `-${OFFSET}px`) : undefined)};
    width: ${(props) => (props.$view === "live" && !props.$isPage ? `calc(100% + ${OFFSET * 2}px);` : "100%")};
    height: ${(props) => (props.$view === "live" && !props.$isPage ? 0 : "100%")};
    margin-right: ${(props) => (props.$view === "live" && !props.$isPage && `-${OFFSET * 2}px`) || undefined};
    margin-bottom: ${(props) => (props.$view === "live" && !props.$isPage && `-${OFFSET * 2}px`) || undefined};
`;

const RootBody = createGlobalStyle<{
    $customCSS?: string;
}>`
    body {
        overflow: hidden;
        background-color: transparent;

        ${(props) =>
            props.$customCSS &&
            css`
                ${Str.replace(props.$customCSS, "tripetto-block-", "@tripetto/block-")}
            `}
    }
`;

const RootElement = styled.div<{
    $view: TRunnerViews;
    $isPage: boolean;
    $styles: IRuntimeStyles;
    $fontFamily: string;
}>`
    position: absolute;
    left: ${(props) => (props.$view === "live" && !props.$isPage ? `${OFFSET}px` : 0)};
    right: ${(props) => (props.$view === "live" && !props.$isPage ? `${OFFSET}px` : 0)};
    top: ${(props) => (props.$view === "live" && !props.$isPage ? `${OFFSET}px` : 0)};
    bottom: ${(props) => (props.$view === "live" && !props.$isPage ? `${OFFSET}px` : 0)};
    font-family: ${(props) => props.$fontFamily};
    font-size: ${(props) => props.$styles.font.size}px;
    font-variant-ligatures: none;
    background-color: ${(props) => color(props.$styles.background.color)};
    background-image: ${(props) =>
        (props.$styles.background.url &&
            props.$styles.background.opacity > 0 &&
            `${
                (props.$styles.background.opacity < 1 &&
                    `linear-gradient(${color(props.$styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - props.$styles.background.opacity))
                    )},${color(props.$styles.background.color, (o) =>
                        o.manipulate((m) => m.alpha(1 - props.$styles.background.opacity))
                    )}),`) ||
                ""
            }url("${props.$styles.background.url}")`) ||
        undefined};
    background-size: ${(props) =>
        (props.$styles.background.url && props.$styles.background.positioning !== "repeat" && props.$styles.background.positioning) ||
        undefined};
    background-repeat: ${(props) =>
        props.$styles.background.url && props.$styles.background.positioning === "repeat" ? "repeat" : "no-repeat"};
    background-position: center center;
    line-height: 1.5em;
    overflow-x: ${(props) => (props.$view === "live" && !props.$isPage ? "visible" : "hidden")};
    overflow-y: ${(props) => (props.$view === "live" && !props.$isPage ? "visible" : "auto")};
    scroll-behavior: smooth;
    -webkit-overflow-scrolling: touch;
    -webkit-tap-highlight-color: transparent;

    > div {
        max-width: ${(props) => props.$isPage && "1140px"};
        margin-left: ${(props) => props.$isPage && "auto"};
        margin-right: ${(props) => props.$isPage && "auto"};
        padding: ${(props) => (props.$isPage || props.$view !== "live" ? `${MARGIN}px` : undefined)};

        > ${Blocks} + ${Blocks} {
            margin-top: ${MARGIN / SIZE}em;
        }
    }

    * {
        font-family: ${(props) => props.$fontFamily};
        font-variant-ligatures: none;
        box-sizing: border-box;
        outline: none;
    }

    a {
        text-decoration: underline;
        color: inherit !important;
    }

    @media (prefers-reduced-motion: reduce) {
        scroll-behavior: auto;
    }

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        font-size: ${(props) => props.$styles.font.sizeSmall}px;

        > div {
            padding: ${(props) => (props.$isPage || props.$view !== "live" ? `${SMALL_SCREEN_MARGIN}px` : undefined)};
        }
    }
`;

const Content = (props: { readonly children?: ReactNode; readonly onChange: (height: number) => void }) => {
    const contentRef = useRef<HTMLElement>() as MutableRefObject<HTMLDivElement>;

    useEffect(() => {
        const ref = attach(() => {
            if (contentRef.current) {
                props.onChange(contentRef.current.getBoundingClientRect().height);
            }
        });

        return () => detach(ref);
    });

    return <div ref={contentRef}>{props.children}</div>;
};

export const ClassicRoot = (props: {
    readonly frameRef: MutableRefObject<HTMLIFrameElement>;
    readonly view: TRunnerViews;
    readonly isPage: boolean;
    readonly title?: string;
    readonly styles: IRuntimeStyles;
    readonly className?: string;
    readonly customStyle?: CSSProperties;
    readonly customCSS?: string;
    readonly children?: ReactNode;
    readonly onTouch?: () => void;
}) => {
    const [contentHeight, setContentHeight] = useState(0);

    useEffect(() => {
        if (props.frameRef.current && props.view === "live" && (props.isPage || props.styles.autoFocus)) {
            props.frameRef.current.focus();
        }
    }, [contentHeight]);

    return (
        <RootFrame
            frameRef={props.frameRef}
            title={props.title}
            style={{
                ...props.customStyle,
                height:
                    props.customStyle && props.customStyle.height && props.customStyle.height !== "auto"
                        ? props.customStyle.height
                        : (props.view === "live" && !props.isPage && contentHeight + OFFSET * 2) || undefined,
            }}
            font={props.styles.font && props.styles.font.family}
            className={props.className}
            onTouch={props.onTouch}
            $view={props.view}
            $isPage={props.isPage}
            $styles={props.styles}
        >
            {(doc: Document, fontFamily: string) => (
                <StyleSheetManager target={doc.head}>
                    <>
                        <RootBody $customCSS={props.customCSS} />
                        <RootElement $view={props.view} $isPage={props.isPage} $styles={props.styles} $fontFamily={fontFamily}>
                            <Content
                                onChange={(height: number) => {
                                    height = Num.ceil(height);

                                    if (height !== contentHeight) {
                                        setContentHeight(height);
                                    }
                                }}
                            >
                                {props.children}
                            </Content>
                        </RootElement>
                    </>
                </StyleSheetManager>
            )}
        </RootFrame>
    );
};
