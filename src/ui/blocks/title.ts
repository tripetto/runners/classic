import { styled } from "styled-components";

export const BlockTitle = styled.h2<{
    $alignment?: "left" | "center" | "right";
}>`
    margin: 0;
    padding: 0;
    font-size: 2em;
    line-height: 1.1em;
    text-align: ${(props) => props.$alignment || "left"};
`;
