import { useState } from "react";
import { L10n } from "@tripetto/runner";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { verifyEmail } from "@helpers/verify";
import { IPausingRecipeEmail } from "@interfaces/pausing";
import { IRuntimeStyles } from "@hooks/styles";
import { Block, Blocks } from ".";
import { BlockTitle } from "./title";
import { BlockDescription } from "./description";
import { EmailFabric } from "@tripetto/runner-fabric/components/email";
import { ButtonFabric } from "@tripetto/runner-fabric/components/button";
import { MailIcon } from "@ui/icons/mail";
import { CancelIcon } from "@ui/icons/cancel";
import { Buttons } from "@ui/navigation/buttons";

const PauseForm = (props: {
    readonly controller: IPausingRecipeEmail;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
}) => {
    const [value, setValue] = useState(props.controller?.emailAddress || "");
    const [hadFocus, setFocus] = useState(false);
    const isValid = verifyEmail(value);

    return (
        <Blocks $styles={props.styles} $view={props.view}>
            <Block $marginAroundBlock={true}>
                <BlockTitle>{props.l10n.pgettext("runner#8|⏸ Pause conversation", "Pause this conversation")}</BlockTitle>
                <BlockDescription>
                    {props.l10n.pgettext(
                        "runner#8|⏸ Pause conversation",
                        "Receive a link by email to resume later on any device, right where you left off."
                    )}
                </BlockDescription>
                <EmailFabric
                    styles={props.styles.inputs}
                    required={true}
                    value={value}
                    placeholder={props.l10n.pgettext("runner#8|⏸ Pause conversation", "Your email address...")}
                    error={hadFocus && !isValid}
                    disabled={props.controller.isCompleting}
                    onChange={setValue}
                    onFocus={() => setFocus(true)}
                    onAutoFocus={(el) => {
                        if (el) {
                            el.focus();
                        }
                    }}
                    onSubmit={() => isValid && props.controller.complete(value)}
                />
                <Buttons>
                    <ButtonFabric
                        styles={props.styles.buttons}
                        icon={MailIcon}
                        label={
                            props.controller && props.controller.isCompleting
                                ? props.l10n.pgettext("runner#3|🩺 Status information", "Pausing...")
                                : props.l10n.pgettext("runner#8|⏸ Pause conversation", "Receive resume link")
                        }
                        disabled={props.controller.isCompleting || !isValid}
                        onClick={() => props.controller.complete(value)}
                    />
                    <ButtonFabric
                        styles={{ ...props.styles.buttons, mode: "outline" }}
                        icon={CancelIcon}
                        disabled={props.controller.isCompleting}
                        label={props.l10n.pgettext("runner#8|⏸ Pause conversation", "Cancel pausing")}
                        onClick={() => props.controller.cancel()}
                    />
                </Buttons>
            </Block>
        </Blocks>
    );
};

export const PauseBlock = (props: {
    readonly controller: IPausingRecipeEmail | undefined;
    readonly l10n: L10n.Namespace;
    readonly styles: IRuntimeStyles;
    readonly view: TRunnerViews;
}) => (props.controller && <PauseForm controller={props.controller} l10n={props.l10n} styles={props.styles} view={props.view} />) || <></>;
