import { styled, css } from "styled-components";
import { TRunnerViews } from "@tripetto/runner-react-hook";
import { MARGIN, SIZE, SMALL_SCREEN_MARGIN, SMALL_SCREEN_SIZE, SPACING } from "@ui/const";
import { IRuntimeStyles } from "@hooks/styles";

export const Blocks = styled.div<{
    $styles: IRuntimeStyles;
    $view: TRunnerViews;
    $center?: boolean;
    $size?: number;
}>`
    display: ${(props) => (props.$center ? "flex" : "block")};
    min-height: ${(props) => props.$center && `calc(100% - ${MARGIN * 2}px)`};
    color: ${(props) => props.$styles.font.color};
    justify-content: ${(props) => props.$center && "center"};
    align-items: ${(props) => props.$center && "center"};
    font-size: ${(props) => (props.$size && `${props.$size}px`) || undefined};
    line-height: ${(props) => (props.$size && "1.5em") || undefined};
    flex-direction: column;

    @media (${(props) => (props.$view === "live" ? "max-device-width" : "max-width")}: ${SMALL_SCREEN_SIZE}px) {
        min-height: ${(props) => props.$center && `calc(100% - ${SMALL_SCREEN_MARGIN * 2}px)`};
    }

    ${(props) =>
        props.$center &&
        css`
            > div {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 100%;
            }
        `}
`;

export const Block = styled.div<{
    $marginAroundBlock?: boolean;
    $locked?: boolean;
}>`
    position: relative;
    margin-top: ${(props) => (props.$marginAroundBlock ? MARGIN : SPACING) / SIZE}em;
    margin-bottom: ${(props) => props.$marginAroundBlock && `${MARGIN / SIZE}em`};
    opacity: ${(props) => (props.$locked && 0.2) || undefined};
    pointer-events: ${(props) => (props.$locked && "none") || undefined};
    max-width: 100%;

    &:first-child {
        margin-top: 0;
    }

    &:last-child {
        margin-bottom: 0;
    }

    > * {
        margin-top: ${8 / SIZE}em !important;

        &:first-child {
            margin-top: 0 !important;
        }
    }
`;
