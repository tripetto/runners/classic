import { styled } from "styled-components";
import { onLoad } from "@helpers/resizer";

const ImageContainer = styled.div<{
    $aligment: "left" | "center" | "right";
}>`
    width: 100%;
    display: flex;
    justify-content: ${(props) => props.$aligment};
`;

const ImageElement = styled.img<{
    $isPage: boolean;
}>`
    margin: 0;
    padding: 0;
    border: none;
    max-width: 100%;
    max-height: ${(props) => props.$isPage && "50vh"};
`;

export const BlockImage = (props: {
    readonly src: string;
    readonly isPage: boolean;
    readonly width?: string;
    readonly alignment?: "left" | "center" | "right";
    readonly onClick?: () => void;
}) => (
    <ImageContainer onClick={props.onClick} $aligment={props.alignment || "left"}>
        <ImageElement src={props.src} width={props.width} onLoad={onLoad} $isPage={props.isPage} />
    </ImageContainer>
);
