import { styled } from "styled-components";

export const BlockName = styled.label<{
    $hasDescription: boolean;
}>`
    font-weight: ${(props) => props.$hasDescription && "bold"};
`;
