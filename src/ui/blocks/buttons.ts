import { styled } from "styled-components";
import { SIZE } from "@ui/const";

export const BlockButtons = styled.nav<{
    $alignment?: "left" | "center" | "right";
}>`
    margin-top: 0 !important;
    min-height: ${40 / SIZE}em;
    display: flex;
    flex-wrap: wrap;
    justify-content: ${(props) => props.$alignment || "left"};

    > * {
        margin-top: ${16 / SIZE}em !important;
        margin-left: ${16 / SIZE}em;

        &:first-child {
            margin-left: 0;
        }
    }
`;
