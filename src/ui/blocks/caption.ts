import { styled } from "styled-components";

export const BlockCaption = styled.h3<{
    $alignment?: "left" | "center" | "right";
}>`
    margin: 0;
    padding: 0;
    font-size: 1.5em;
    line-height: 1.1em;
    text-align: ${(props) => props.$alignment || "left"};
`;
