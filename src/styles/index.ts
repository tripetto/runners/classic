import { TStylesContract } from "@tripetto/runner";

declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

export default (pgettext: (context: string, id: string, ...args: string[]) => string): TStylesContract => ({
    contract: {
        name: PACKAGE_NAME,
        version: PACKAGE_VERSION,
    },
    styles: [
        {
            elements: [
                {
                    type: "color",
                    name: "color",
                    label: pgettext("runner:classic", "Color"),
                },
            ],
        },
        {
            title: pgettext("runner:classic", "Font"),
            optional: true,
            name: "font",
            elements: [
                {
                    type: "static",
                    label: pgettext(
                        "runner:classic",
                        "Use any font available on [Google Fonts](https://fonts.google.com/) or supply a custom font URL. Please note that the Google Fonts name is case sensitive."
                    ),
                },
                {
                    type: "dropdown",
                    name: "family",
                    label: pgettext("runner:classic", "Font family"),
                    options: [
                        {
                            optGroup: pgettext("runner:classic", "Standard fonts"),
                        },
                        {
                            label: pgettext("runner:classic", "Default"),
                            value: "",
                        },
                        {
                            label: "Arial",
                            value: "Arial",
                        },
                        {
                            label: "Arial Black",
                            value: "Arial Black",
                        },
                        {
                            label: "Comic Sans MS",
                            value: "Comic Sans MS",
                        },
                        {
                            label: "Courier New",
                            value: "Courier New",
                        },
                        {
                            label: "Georgia",
                            value: "Georgia",
                        },
                        {
                            label: "Garamond",
                            value: "Garamond",
                        },
                        {
                            label: "Helvetica",
                            value: "Helvetica",
                        },
                        {
                            label: "Impact",
                            value: "Impact",
                        },
                        {
                            label: "Palatino",
                            value: "Palatino",
                        },
                        {
                            label: "Sans-Serif",
                            value: "Sans-Serif",
                        },
                        {
                            label: "Times New Roman",
                            value: "Times New Roman",
                        },
                        {
                            label: "Trebuchet MS",
                            value: "Trebuchet MS",
                        },
                        {
                            label: "Verdana",
                            value: "Verdana",
                        },
                        {
                            optGroup: pgettext("runner:classic", "Custom fonts"),
                        },
                        {
                            custom: pgettext("runner:classic", "Google Fonts or URL"),
                        },
                    ],
                },
                {
                    type: "number",
                    name: "size",
                    label: pgettext("runner:classic", "Font size"),
                    default: 14,
                    min: 8,
                    max: 30,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "number",
                    name: "sizeSmall",
                    label: pgettext("runner:classic", "Font size for small screens"),
                    default: 14,
                    min: 8,
                    max: 30,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "static",
                    label: pgettext(
                        "runner:classic",
                        "_Note_: This size applies to small screens (like mobile devices). Switch to the mobile device preview to see and test this small font size."
                    ),
                },
            ],
        },
        {
            title: pgettext("runner:classic", "Background"),
            optional: true,
            name: "background",
            elements: [
                {
                    type: "color",
                    name: "color",
                    label: pgettext("runner:classic", "Color"),
                },
                {
                    type: "image",
                    name: "url",
                    label: pgettext("runner:classic", "Image (URL)"),
                },
                {
                    type: "number",
                    name: "opacity",
                    label: pgettext("runner:classic", "Opacity"),
                    min: 0,
                    max: 100,
                    suffix: "%",
                    default: 100,
                    dependency: {
                        action: "enable",
                        observe: [
                            {
                                property: "background.color",
                                defined: true,
                            },
                            {
                                property: "background.url",
                                defined: true,
                            },
                        ],
                    },
                },
                {
                    type: "dropdown",
                    name: "positioning",
                    label: pgettext("runner:classic", "Positioning"),
                    options: [
                        {
                            label: pgettext("runner:classic", "Center"),
                            value: "auto",
                        },
                        {
                            label: pgettext("runner:classic", "Repeat"),
                            value: "repeat",
                        },
                        {
                            label: pgettext("runner:classic", "Cover (full screen)"),
                            value: "cover",
                        },
                        {
                            label: pgettext("runner:classic", "Contain (full image stretched over screen)"),
                            value: "contain",
                        },
                        {
                            label: pgettext("runner:classic", "100% width, height centered"),
                            value: "100% auto",
                        },
                        {
                            label: pgettext("runner:classic", "100% height, width centered"),
                            value: "auto 100%",
                        },
                    ],
                    default: "auto",
                    dependency: {
                        action: "disable",
                        observe: [
                            {
                                property: "background.url",
                                value: "",
                            },
                        ],
                    },
                },
            ],
        },
        {
            title: pgettext("runner:classic", "Inputs"),
            optional: true,
            name: "inputs",
            elements: [
                {
                    type: "color",
                    name: "backgroundColor",
                    label: pgettext("runner:classic", "Background color"),
                },
                {
                    type: "color",
                    name: "borderColor",
                    label: pgettext("runner:classic", "Border color"),
                },
                {
                    type: "number",
                    name: "borderSize",
                    label: pgettext("runner:classic", "Border size"),
                    default: 1,
                    min: 1,
                    max: 20,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "number",
                    name: "roundness",
                    label: pgettext("runner:classic", "Roundness"),
                    default: 8,
                    min: 0,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "color",
                    name: "textColor",
                    label: pgettext("runner:classic", "Text color"),
                },
                {
                    type: "color",
                    name: "errorColor",
                    label: pgettext("runner:classic", "Required/error color"),
                    default: "red",
                },
                {
                    type: "color",
                    name: "selectionColor",
                    label: pgettext("runner:classic", "Selection color (input buttons)"),
                },
                {
                    type: "color",
                    name: "agreeColor",
                    label: pgettext("runner:classic", "Agree color (input buttons)"),
                    default: "green",
                },
                {
                    type: "color",
                    name: "declineColor",
                    label: pgettext("runner:classic", "Decline color (input buttons)"),
                    default: "red",
                },
            ],
        },
        {
            title: pgettext("runner:classic", "Buttons"),
            optional: true,
            name: "buttons",
            elements: [
                {
                    type: "color",
                    name: "baseColor",
                    label: pgettext("runner:classic", "Button color"),
                },
                {
                    type: "color",
                    name: "textColor",
                    label: pgettext("runner:classic", "Text color"),
                },
                {
                    type: "dropdown",
                    name: "mode",
                    label: pgettext("runner:classic", "Style"),
                    options: [
                        {
                            label: pgettext("runner:classic", "Fill"),
                            value: "fill",
                        },
                        {
                            label: pgettext("runner:classic", "Outline"),
                            value: "outline",
                        },
                    ],
                },
                {
                    type: "number",
                    name: "roundness",
                    label: pgettext("runner:classic", "Roundness"),
                    default: 8,
                    min: 0,
                    suffix: "px",
                    width: 75,
                },
                {
                    type: "color",
                    name: "finishColor",
                    label: pgettext("runner:classic", "Submit button color"),
                },
            ],
        },
        {
            title: pgettext("runner:classic", "Appearance"),
            additional: true,
            elements: [
                {
                    type: "radiobuttons",
                    name: "mode",
                    buttons: [
                        {
                            label: pgettext("runner:classic", "Progressive"),
                            value: "progressive",
                            description: pgettext("runner:classic", "Present the whole form at once."),
                        },
                        {
                            label: pgettext("runner:classic", "Continuous"),
                            value: "continuous",
                            description: pgettext("runner:classic", "Keep answered blocks in view while working through the form."),
                        },
                        {
                            label: pgettext("runner:classic", "Paginated"),
                            value: "paginated",
                            description: pgettext(
                                "runner:classic",
                                "Use pagination for the form and let the user navigate through the form using the next and back buttons."
                            ),
                        },
                    ],
                    default: "progressive",
                },
            ],
        },
        {
            title: pgettext("runner:classic", "Options"),
            additional: true,
            elements: [
                {
                    type: "checkbox",
                    name: "autoFocus",
                    label: pgettext("runner:classic", "Automatically gain focus"),
                    description: pgettext("runner:classic", "Only applies when embedding the form."),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "showEnumerators",
                    label: pgettext("runner:classic", "Display numbering"),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "hideRequiredIndicator",
                    label: pgettext("runner:classic", "Hide asterisk for required questions"),
                    default: false,
                },
                {
                    type: "checkbox",
                    name: "showPageIndicators",
                    label: pgettext("runner:classic", "Display page indicators"),
                    description: pgettext("runner:classic", "Only available for paginated forms."),
                    default: false,
                    dependency: {
                        observe: {
                            property: "mode",
                            value: "paginated",
                        },
                        action: "enable",
                    },
                },
                {
                    type: "checkbox",
                    name: "showProgressbar",
                    label: pgettext("runner:classic", "Display progressbar"),
                    description: pgettext("runner:classic", "Only available for paginated forms."),
                    default: false,
                    dependency: {
                        observe: {
                            property: "mode",
                            value: "paginated",
                        },
                        action: "enable",
                    },
                },
                {
                    type: "checkbox",
                    name: "noBranding",
                    label: pgettext("runner:classic", "Hide all the Tripetto branding"),
                    default: false,
                    tier: "licensed",
                },
            ],
        },
    ],
});
