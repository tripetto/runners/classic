const fs = require("fs");
const path = "./runner/types/index.d.ts";
const banner = require("../banner/banner.js");
const declaration = fs.readFileSync(path, "utf8").split("\n");
let errors = 4;

for (let row = 0; row < declaration.length; row++) {
    declaration[row] = declaration[row].replace("declare ", "");

    if (declaration[row].indexOf("private ") !== -1) {
        if (row > 0 && declaration[row - 1].indexOf("*/") !== -1) {
            let comment = row - 1;

            while (comment > 0 && declaration[comment].indexOf("/*") === -1) {
                declaration[comment] = "";

                comment--;
            }

            declaration[comment] = "";
        }

        declaration[row] = "";
    }

    if (declaration[row].indexOf("export {};") !== -1) {
        declaration[row] = "";
        errors--;
    }

    if (
        declaration[row].indexOf(
            `import { Hooks, IDefinition, ISnapshot, Instance, NodeBlock, TL10n, TStyles } from '@tripetto/runner';`
        ) !== -1
    ) {
        declaration[row] = `import { Hooks, IDefinition, ISnapshot, Instance, L10n, NodeBlock, TL10n, TStyles } from '@tripetto/runner';`;
        errors--;
    }

    if (
        declaration[row].indexOf(
            `import { IRunnerAttachments, IRunnerProps, TRunnerPreviewData, TRunnerViews } from '@tripetto/runner-react-hook';`
        ) !== -1
    ) {
        declaration[row] += `\nimport { styled } from "styled-components";\n`;
        errors--;
    }

    if (declaration[row].indexOf(`export { css, keyframes } from "styled-components";`) !== -1) {
        declaration[row] = `export { styled };\n${declaration[row]}`;
        errors--;
    }
}

if (errors !== 0) {
    throw new Error("Declaration generation failed!");
}

const header = [
    `declare module "@tripetto/runner-classic" {`,
    `import * as TripettoClassic from "@tripetto/runner-classic/module";`,
    `export * from "@tripetto/runner-classic/module";`,
    `export { TripettoClassic };`,
    `export default TripettoClassic;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-classic/es5" {`,
    `import * as TripettoClassic from "@tripetto/runner-classic/module";`,
    `export * from "@tripetto/runner-classic/module";`,
    `export { TripettoClassic };`,
    `export default TripettoClassic;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-classic/runner" {`,
    `import * as TripettoClassic from "@tripetto/runner-classic/module";`,
    `export * from "@tripetto/runner-classic/module";`,
    `export { TripettoClassic };`,
    `export default TripettoClassic;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-classic/runner/es5" {`,
    `import * as TripettoClassic from "@tripetto/runner-classic/module";`,
    `export * from "@tripetto/runner-classic/module";`,
    `export { TripettoClassic };`,
    `export default TripettoClassic;`,
    `}`,
    `\n`,
    `declare module "@tripetto/runner-classic/react" {`,
    `import * as TripettoClassic from "@tripetto/runner-classic/module";`,
    `export * from "@tripetto/runner-classic/module";`,
    `export { TripettoClassic };`,
    `export default TripettoClassic;`,
    `}`,
];

fs.writeFileSync(
    path,
    `/*! ${banner} */\n\n` +
        `${header.join("\n")}\n\n` +
        `declare module "@tripetto/runner-classic/module" {\n${declaration.join("\n")}\n}\n`,
    "utf8"
);
