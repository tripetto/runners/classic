## <a href="https://tripetto.com/sdk/"><img src="https://unpkg.com/@tripetto/builder/assets/header.svg" alt="Tripetto FormBuilder SDK"></a>

🙋‍♂️ The *Tripetto FormBuilder SDK* helps building **powerful and deeply customizable forms for your application, web app, or website.**

👩‍💻 Create and run forms and surveys **without depending on external services.**

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [Fortune 500 companies](https://en.wikipedia.org/wiki/Fortune_500).

---

*This SDK is the ultimate form solution for everything from basic contact forms to surveys, quizzes and more with intricate flow logic. Whether you're just adding conversational forms to your website or application, or also need visual form-building capabilities inside your app, Tripetto has got you covered! Pick what you need from the SDK with [visual form builder](https://tripetto.com/sdk/docs/builder/introduction/), [form runners](https://tripetto.com/sdk/docs/runner/introduction/), and countless [question types](https://tripetto.com/sdk/docs/blocks/introduction/) – all with [extensive docs](https://tripetto.com/sdk/docs/). Or take things up a notch by developing your [own question types](https://tripetto.com/sdk/docs/blocks/custom/introduction/) or even [form runner UIs](https://tripetto.com/sdk/docs/runner/custom/introduction/).*

---

## 📦 Tripetto Classic Form Runner
[![Version](https://badgen.net/npm/v/@tripetto/runner-classic?icon=npm&label)](https://www.npmjs.com/package/@tripetto/runner-classic)
[![Downloads](https://badgen.net/npm/dt/@tripetto/runner-classic?icon=libraries&label)](https://www.npmjs.com/package/@tripetto/runner-classic)
[![Typings included](https://badgen.net/badge/icon/typings%20included/blue?icon=typescript&label)](https://tripetto.com/sdk/docs/runner/stock/api/classic/)
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/runner/stock/faces/classic/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/runners/classic/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

This package is a classic form UI for running Tripetto forms and surveys. In this UI the questions and elements (so-called blocks) are displayed in a traditional way.

## 📺 Preview
[![Showcase](https://unpkg.com/@tripetto/runner-classic/assets/preview.gif)](https://tripetto.gitlab.io/runners/classic/)

[![Play around](https://unpkg.com/@tripetto/builder/assets/button-demo.svg)](https://tripetto.gitlab.io/runners/classic/)

## 🚀 Quickstart
[![Implement runner using plain JS](https://unpkg.com/@tripetto/builder/assets/button-js.svg)](https://tripetto.com/sdk/docs/runner/stock/quickstart/plain-js/)
[![Implement runner using React](https://unpkg.com/@tripetto/builder/assets/button-react.svg)](https://tripetto.com/sdk/docs/runner/stock/quickstart/react/)
[![Implement runner using Angular](https://unpkg.com/@tripetto/builder/assets/button-angular.svg)](https://tripetto.com/sdk/docs/runner/stock/quickstart/angular/)
[![Implement runner using HTML](https://unpkg.com/@tripetto/builder/assets/button-html.svg)](https://tripetto.com/sdk/docs/runner/stock/quickstart/html/)

## 📖 Documentation
Tripetto has practical, extensive documentation. Find everything you need at [tripetto.com/sdk/docs/](https://tripetto.com/sdk/docs/).

## 🆘 Support
Run into issues or bugs? Report them [here](https://gitlab.com/tripetto/runners/classic/-/issues).

Need help or assistance? Please go to our [support page](https://tripetto.com/sdk/support/). We're more than happy to help you.

## 💳 License
[![License](https://badgen.net/npm/license/@tripetto/runner-classic?icon=libraries&label)](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)

You are allowed to use this package without a license (and free of charge) as long as you keep the Tripetto branding enabled (which is the default mode). When you want to [remove the branding](https://tripetto.com/sdk/docs/runner/stock/guides/branding/), you need a (paid) [license](https://tripetto.com/sdk/pricing/) to do so.

## ✨ Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
